<?php

Broadcast::channel('Scrola.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
