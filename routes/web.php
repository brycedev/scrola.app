<?php

Route::group(['middleware' => 'http2'], function() {
  Route::get('/', function () {
      return view('index');
  });

  Route::get('/tos', function () {
      return view('tos');
  });

  Route::get('/dashboard', function () {
      return view('dash');
  })->middleware('auth');

  Route::get('/dashboard/{path}', function(){
      return view('dash');
  })->middleware('auth');

  Auth::routes();
});

Route::group(['middleware' => 'auth'], function() {
    Route::get('/api/videos', 'VideoController@videos');
    Route::get('/api/videos/{id}', 'VideoController@download');
    Route::get('/api/videos/preview/{id}', 'VideoController@preview');
    Route::post('/api/videos', 'VideoController@create');
    Route::delete('/api/videos/{id}', 'VideoController@delete');
    Route::post('/api/videos/screenshot', 'VideoController@screenshot');
    Route::get('/api/user', 'UserController@ping');
    Route::delete('/api/user', 'UserController@delete');
    Route::post('/api/user/settings', 'UserController@settings');
    Route::post('/api/subscription', 'SubscriptionController@create');
    Route::delete('/api/subscription', 'SubscriptionController@cancel');
    Route::post('/api/subscription/card', 'SubscriptionController@card');
    Route::post('/api/user/logout', 'UserController@logout');
});

Route::post('stripe/webhook','StripeWebhookController@handleWebhook');
