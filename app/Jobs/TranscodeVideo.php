<?php

namespace Scrola\Jobs;

use App;
use Exception;
use Chumper\Zipper\Zipper;
use Scrola\Models\Video;
use Scrola\Notifications\VideoTranscoded;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Process;

class TranscodeVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $video;

    public function __construct(Video $video)
    {
        $this->data = $video->data;
        $this->video = $video;
    }

    public function handle()
    {
        $command = App::environment('production') ? '/usr/bin/webcord ' : '/usr/local/bin/webcord ';
        if($this->data['from_screenshot']){
            $command.= '-i ' . storage_path(str_replace('/storage/', 'app/public/', $this->data['screenshot']));
        } else {
            $command.= '-u ' . $this->data['url'];
        }
        $command.= ' -r ' . $this->data['rate'];
        $command.= ' -v ' . $this->data['viewport'];
        if(!$this->video->user->subscribed('main')){
          $command.= ' -w ' . storage_path('watermark.png');
        }
        if ($this->data['background']) {
          $command.= ' -b ' . $this->data['background_color'];
          $command.= ' -p ' . $this->data['position'];
        }
        if ($this->data['should_loop']) {
          $command.= ' -l';
        }
        if($this->video->is_collection){
          $command.= ' -c';
        }
        \Log::info($command);
        $process = new Process($command, storage_path($this->video->path));
        $process->setTimeout(3400);
        $process->run(function($type, $buffer){
            if(substr_count($buffer, 'Stderr output') == 0){
                \Log::info($buffer);
            }
        });
        if($process->isSuccessful()){
            if($this->video->is_collection){
                $files = glob(storage_path($this->video->path));
                $zipper = new Zipper;
                $zipper->make(storage_path($this->video->path) . '/video.zip')->add($files)->close();
            }
            $this->video->is_transcoded = true;
        } else {
            $this->video->is_transcoded = false;
            $this->video->is_perma_failed = true;
        }
        $this->video->save();
        $this->video->user->notify(new VideoTranscoded($this->video));
    }

    public function failed(Exception $exception)
    {
        $this->video->is_perma_failed = true;
        $this->video->save();
        $this->video->user->notify(new VideoTranscoded($this->video));
    }
}
