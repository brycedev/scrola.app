<?php

namespace Scrola\Http\Controllers\Auth;

use Scrola\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/dashboard/screenshots';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
