<?php

namespace Scrola\Http\Controllers\Auth;

use Auth;
use Scrola\Models\User;
use Scrola\Notifications\UserRegistered;
use Scrola\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/dashboard/screenshots';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|min:3',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
    }

    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'preferences' => [
                'notifications' => [
                    'email' => true
                ]
            ]
        ]);
        Auth::loginUsingId($user->id);
		$user->notify(new UserRegistered($user));
		return $user;
    }
}
