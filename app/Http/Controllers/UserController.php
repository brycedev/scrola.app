<?php

namespace Scrola\Http\Controllers;

use Auth;
use Hash;
use Storage;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function delete(Request $request)
    {
        $user = $request->user();
        $videos = $user->videos()->withTrashed()->get();
        $videos->each(function($i, $k) {
          Storage::delete($i->path);
          $i->forceDelete();
        });
        if($user->subscribed('main')) $user()->subscription('main')->cancel();
        $user->delete();
        return response()->json(['message' => 'success']);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return response()->json(['message' => 'success']);
    }

    public function ping(Request $request)
    {
        return response()->json($request->user());
    }

    public function settings(Request $request)
    {
        $user = $request->user();
        if($request->has('password')){
            $user->password = Hash::make($request->password);
        }
        if($request->has('name')){
            $user->name = $request->name;
        }
        if($request->has('email')){
            $user->email = $request->email;
        }
        if($request->has('preferences')){
            $user->preferences = $request->preferences;
        }
        $user->save();
        return response()->json(['message' => 'success']);
    }
}
