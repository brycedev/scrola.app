<?php

namespace Scrola\Http\Controllers;

use File;
use Storage;
use Illuminate\Http\Request;
use Scrola\Models\Video;
use Scrola\Jobs\TranscodeVideo;
use Symfony\Component\Process\Process;
use GuzzleHttp\{Client, Psr7};
use GuzzleHttp\Exception\RequestException;

class VideoController extends Controller
{
    public function videos(Request $request)
    {
        $user = $request->user();
        return $user->videos;
    }

    public function create(Request $request)
    {
        $user = $request->user();
        $data = $request->except(['image']);
        $data['background'] = filter_var($data['background'], FILTER_VALIDATE_BOOLEAN);
        $data['from_screenshot'] = filter_var($data['from_screenshot'], FILTER_VALIDATE_BOOLEAN);
        $data['should_loop'] = filter_var($data['should_loop'], FILTER_VALIDATE_BOOLEAN);
        if ($request->has('image')) {
            $file = md5(uniqid() . microtime()) . '.png';
            $path = Storage::putFileAs('public/tmp', $request->image, $file);
            $data['screenshot'] = str_replace('public', '/storage', $path);
        } else {
            $data['screenshot'] = str_replace('app/public', '/storage', $data['screenshot']);
        }
        $video = Video::create([
            'data' => $data,
            'user_id' => $user ? $user->id : null,
            'is_collection' => $user->is_subscribed
        ]);
        $storageBasePath = 'videos/' . $user->id . '/' . $video->id;
        $video->path = $storageBasePath;
        $video->save();
        File::makeDirectory(storage_path($storageBasePath), $mode = 0775, true, true);
        TranscodeVideo::dispatch(Video::find($video->id))->onQueue('transcoding');
        return response()->json(['message' => 'success']);
    }

    public function fromExtension(Request $request)
    {
        // $video = Video::create([
        //     'data' => $data,
        //     'user_id' => $user ? $user->id : null,
        //     'is_collection' => $user->is_subscribed
        // ]);
        $storageBasePath = 'videos/extension/' . substr(\Hash::make(now()), 0, 8);
        File::makeDirectory(storage_path($storageBasePath), $mode = 0775, true, true);
        // TranscodeVideo::dispatch(Video::find($video->id))->onQueue('transcoding');
        return response()->json(['message' => 'success']);
    }

    public function delete(Request $request, $id)
    {
        $user = $request->user();
        $video = Video::find($id);
        if($video->user_id == $user->id){
            Storage::deleteDirectory(storage_path($video->path));
            $video->delete();
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'unauthorized'], 401);
    }

    public function download(Request $request, $id)
    {
      if(strpos($request->server('HTTP_REFERER'), '/dashboard/screenshots')){
        $user = $request->user();
        $video = Video::find($id);
        if($video->user_id == $user->id){
          if($video->is_collection){
              if($video->data['from_screenshot']){
                $title = 'Uploaded Screenshot' . '-' . $video->created_at . '-' . $video->data['viewport'] . '.zip';
              } else {
                $title = parse_url($video->data['url'])['host'] . '-' . $video->data['viewport'] . '.zip';
              }
              return response()->download(storage_path($video->path) . '/video.zip', $title);
          } else {
              if($video->data['from_screenshot']){
                $title = 'Uploaded Screenshot' . '-' . $video->created_at . '-' . $video->data['viewport'] . '.mp4';
              } else {
                $title = parse_url($video->data['url'])['host'] . '-' . $video->data['viewport'] . '.mp4';
              }
              return response()->download(storage_path($video->path) . '/video.mp4', $title, [
                'Content-Type' => 'video/mp4']
              );
          }
        }
        return response()->json(['message' => 'unauthorized'], 401);
      }
      return redirect('/');
    }

    public function preview(Request $request, $id)
    {
        if(strpos($request->server('HTTP_REFERER'), '/dashboard/screenshots')){
            $user = $request->user();
            $video = Video::find($id);
            if($video->user_id == $user->id){
                return response()->make(File::get(storage_path($video->path) . '/video.mp4'), 200);
            } else {
                return response()->json(['message' => 'unauthorized'], 401);
            }
        }
        return response()->json(['message' => 'unauthorized'], 401);
    }

    public function screenshot(Request $request)
    {
        $client = new Client(['verify' => false]);
        try {
          $response = $client->request('GET', $request->url);
          if($response->getStatusCode() !== 200) return response()->json(['message' => 'ping failure'], 500);
        } catch (RequestException $e) {
          return response()->json(['message' => 'ping failure'], 500);
        }
        $file = md5(uniqid() . microtime()) . '.png';
        $command = \App::environment('production') ? 'node /usr/bin/webcord -u ' : 'node /usr/local/bin/webcord -u ';
        $command.= $request->url;
        $command.= ' -v ' . $request->viewport;
        $command.= ' -s ' . $file;
        $process = new Process($command, storage_path('app/public/tmp'));
        $process->run(function($type, $buffer){
            if (substr_count($buffer, 'Stderr output') == 0) {
                \Log::info($buffer);
            }
        });
        if ($process->isSuccessful()) {
            return response()->json(['image' => '/storage/tmp/' . $file]);
        } else {
            return response()->json(['message' => 'failed'], 500);
        }
    }
}
