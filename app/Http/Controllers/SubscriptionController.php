<?php

namespace Scrola\Http\Controllers;

use Illuminate\Http\Request;
use Scrola\Models\User;

class SubscriptionController extends Controller
{
    public function create(Request $request){
        $user = $request->user();
        if (!$user->subscribed('main')) {
            $user->newSubscription('main', $request->plan)->create($request->token, [
                'email' => $user->email
            ]);
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'user already has a subscription'], 500);
    }

    public function card(Request $request){
        $user = $request->user();
        if($user->subscribed('main') || $user->subscription('main')->cancelled()){
            $user()->updateCard($request->token);
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'user does not have a stripe account'], 500);
    }

    public function cancel(Request $request){
        $user = $request->user();
        if ($user->subscribed('main')) {
            $user->subscription('main')->cancel();
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'user does not have a subscription'], 500);
    }
}
