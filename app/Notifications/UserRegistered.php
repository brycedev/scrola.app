<?php

namespace Scrola\Notifications;

use Scrola\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\{MailMessage, SlackMessage};

class UserRegistered extends Notification
{
	protected $user;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return \App::environment('production') ? ['slack'] : [''];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
	}

	public function toSlack($notifiable)
	{
		return (new SlackMessage)
			->from('Scrola')
			->to('#scrola')
			->success()
			->content('New user!')
			->image('https://scrola.app/favicon.png')
			->attachment(function ($attachment) {
				$attachment->title('User ' . $this->user->id, 'https://scrola.app')
					->fields([
						'Email' => $this->user->email,
						'Name' => $this->user->name
					]);
			});
	}

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
