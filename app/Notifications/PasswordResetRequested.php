<?php

namespace Scrola\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordResetRequested extends Notification implements ShouldQueue
{
    use Queueable;

    protected $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $url = url('/password/reset/' . $this->token);
        return (new MailMessage)
            ->greeting(' ')
            ->from('notifications@scrola.app', 'Scrola')
            ->subject('Scrola | Password Reset Requested ')
            ->line('You are receiving this email because we received a password reset request for your account. If you did not request a password reset, no further action is required.')
            ->action('Reset Password', $url);
    }

    public function toArray($notifiable)
    {
        return [];
    }
}
