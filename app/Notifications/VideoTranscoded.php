<?php

namespace Scrola\Notifications;

use Scrola\Models\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\{MailMessage, SlackMessage};

class VideoTranscoded extends Notification implements ShouldQueue
{
    use Queueable;

    public $video;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    public function via($notifiable)
    {
        $channels = ['broadcast'];
        if(\App::environment('production')) array_push($channels, 'slack');
        if($this->video->user->preferences['notifications']['email']){
          array_push($channels, 'mail');
        }
        return $channels;
    }

    public function toMail($notifiable)
    {
        $message = '';
        if(!$this->video->is_perma_failed){
            $message = 'Your new screenshot is ready! Please login to access the download.';
        } else {
            $message = 'We encountered an issue while creating your screenshot. We apologize for the inconvenience. Please login to try again.';
        }
      	return (new MailMessage)
            ->greeting(' ')
            ->from('notifications@scrola.app', 'Scrola')
            ->subject('Scrola | Processing Complete ')
            ->line($message)
            ->action('Visit Dashboard', url('dashboard/screenshots'));
    }

    public function toSlack($notifiable)
    {
		$data = $this->video->data;
		$message = '';
		if(!$this->video->is_perma_failed){
			$message = 'screenshot created';
		} else {
			$message = 'screenshot failure';
		}
		return(new SlackMessage)
			->from('Scrola')
			->to('#scrola')
			->success()
			->content($message)
			->image('https://scrola.app/favicon.png')
			->attachment(function ($attachment) use ($data) {
				$attachment->title('video ' . $this->video->id, 'https://scrola.app')
				->fields([
					'user' => $this->video->user->email,
					'url' => $data['from_screenshot'] ? 'screenshot' : $data['url'],
					'viewport' => $data['viewport'],
					'speed' => $data['rate'],
				]);
			});
    }

    public function toArray($notifiable)
    {
        return [];
    }
}
