<?php

namespace Scrola\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class PurgeTempshots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrola:purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purge temporary screenshots older than 6 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::files('public/tmp');
        $now = Carbon::now();
        foreach ($files as $file) {
            $then = Carbon::createFromTimestampUTC(Storage::lastModified($file));
            $hours = $now->diffInHours($then);
            if($hours >= 6){
                Storage::delete($file);
            }
        }
    }
}
