<?php

namespace Scrola\Models;

use App;
use Laravel\Cashier\Billable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Scrola\Notifications\PasswordResetRequested;

class User extends Authenticatable
{
    use Billable, Notifiable;

    protected $appends = ['is_on_grace', 'is_subscribed', 'grace_end'];
    protected $casts = ['preferences' => 'array'];
    protected $guarded = [];
    protected $hidden = ['password', 'remember_token'];

    public function getGraceEndAttribute()
    {
        return $this->subscribed('main') && $this->subscription('main')->onGracePeriod()
            ? $this->subscription('main')->ends_at
            : null;
    }

    public function getIsOnGraceAttribute()
    {
        return $this->subscribed('main') && $this->subscription('main')->onGracePeriod();
    }

    public function getIsSubscribedAttribute()
    {
        return $this->subscribed('main');
    }

    public function notifyVia(){
        return 'Scrola.Models.Users.' . $this->id;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetRequested($token));
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
	}

	public function routeNotificationForSlack($notification)
	{
		return 'https://hooks.slack.com/services/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';
	}
}
