let mix = require("laravel-mix")
let glob = require('glob-all')
let tailwindcss = require("tailwindcss")
let PurgecssPlugin = require("purgecss-webpack-plugin")

mix.disableNotifications()
mix.postCss('resources/css/app.css', "public/css", [tailwindcss("./tailwind.js")])
mix.js('resources/js/app.js', 'public/js')

class TailwindExtractor {
  static extract(content) {
    return content.match(/[A-Za-z0-9-_:\/]+/g) || [];
  }
}

if (mix.inProduction()) {
  mix.version()
  mix.webpackConfig({
    plugins: [
      new PurgecssPlugin({
        paths: glob.sync([
          path.join(__dirname, "resources/views/**/*.blade.php"),
          path.join(__dirname, "resources/js/**/*.vue")
        ]),
        extractors: [
          {
            extractor: TailwindExtractor,
            extensions: ["html", "js", "php", "vue"]
          }
        ]
      })
    ]
  });
}

