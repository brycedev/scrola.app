document.addEventListener("DOMContentLoaded", e => {
  const sb = document.getElementById("screenshot")
  sb.onclick = async () => {
    await request()
  }
})

const request = async () => {
  return new Promise((resolve, reject) => {
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, tabs => {
      axios.post('https://scrola.dev/api/extension/videos', { url: tabs[0].url })
      .then(response => {
        resolve(response.data)
      })
      .catch(err => {
        reject(err)
      })
    });
  })
}
