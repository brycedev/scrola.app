<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('data');
            $table->integer('user_id')->nullable();
            $table->string('path')->default('');
            $table->boolean('is_collection')->default(false);
            $table->boolean('is_transcoded')->default(false);
            $table->boolean('is_perma_failed')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
