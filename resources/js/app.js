if(typeof(window.user) == 'undefined')
  window.location.replace('/')

require('./bootstrap')
import VueRouter from 'vue-router'
const Create = require('./components/Create')
const Dash = require('./components/Dash')
const Screenshots = require('./components/Screenshots')
const Settings = require('./components/Settings')

const routes = [{
  path: '/dashboard',
  component: Dash,
  children: [
    { path: 'new', component: Create, name: 'Create' },
    { path: 'screenshots', component: Screenshots, name: 'Screenshots' },
    { path: 'settings', component: Settings, name: 'Settings' },
    { path: '/', redirect: '/dashboard/screenshots' },
    { path: ':any', redirect: '/dashboard/screenshots' }
  ]
}]
const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  el: '#app',
  data: () => ({
    store: {
      bus: new Vue(),
      isDev: process.env.NODE_ENV === "development",
      user: window.user,
      videos: []
    }
  }),
  router
})
