import VueRouter from 'vue-router'
import VueStash from 'vue-stash'
import Message from 'vue-m-message'
import PortalVue from 'portal-vue'
import VTooltip from 'v-tooltip'

window.Vue = require('vue')
window.Vue.use(Message)
window.Vue.use(PortalVue)
window.Vue.use(VueStash)
window.Vue.use(VueRouter)
window.Vue.use(VTooltip)
window.io = require('socket.io-client')
window.axios = require('axios')
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
let token = document.head.querySelector('meta[name="csrf-token"]')
if (token) {
  window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
}

import Echo from 'laravel-echo'

window.Echo = new Echo({
  broadcaster: 'socket.io',
  host: process.env.NODE_ENV === "development"
    ? 'http://localhost:6001'
    : window.location.origin + ':6001'
})
