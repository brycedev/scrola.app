<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <title>Dashboard | Scrola</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,300,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div id="app">
      <router-view></router-view>
    </div>
    <script type="text/javascript">
      window.user = {!! Auth::user() !!}
    </script>
    @if (\App::environment('production'))
      <script type="text/javascript">
        (function(w,d){
          w.HelpCrunch=function(){w.HelpCrunch.q.push(arguments)};w.HelpCrunch.q=[];
          function r(){var s=document.createElement('script');s.async=1;s.type='text/javascript';s.src='https://widget.helpcrunch.com/';(d.body||d.head).appendChild(s);}
          if(w.attachEvent){w.attachEvent('onload',r)}else{w.addEventListener('load',r,false)}
        })(window, document)
      </script>
    @endif
    <script src="{{ mix('js/app.js') }}"></script>
  </body>
</html>
