<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
		<meta charset="utf-8">
		<meta name="title" content="Scrola | Create Scrolling, Website Screenshots">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="crola allows web designers and developers to create scrolling screenshots of websites in just a few clicks. Simply select the viewport, and provide a URL or your own screenshot.">
		<!-- Open Graph / Facebook -->
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://scrola.app/">
		<meta property="og:title" content="Scrola | Create Scrolling, Website Screenshots">
		<meta property="og:description" content="Scrola allows web designers and developers to create scrolling screenshots of websites in just a few clicks. Simply select the viewport, and provide a URL or your own screenshot.">
		<meta property="og:image" content="https://scrola.app/img/mt.jpg">
		<!-- Twitter -->
		<meta property="twitter:card" content="summary_large_image">
		<meta property="twitter:url" content="https://scrola.app/">
		<meta property="twitter:title" content="Scrola | Create Scrolling, Website Screenshots">
		<meta property="twitter:description" content="Scrola allows web designers and developers to create scrolling screenshots of websites in just a few clicks. Simply select the viewport, and provide a URL or your own screenshot.">
		<meta property="twitter:image" content="https://scrola.app/img/mt.jpg">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <title>Scrola | Terms of Service</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css" />
    <style>
    @font-face {
      font-family: system;
      font-style: normal;
      font-weight: 300;
      src: local(".SFNSText-Light"), local(".HelveticaNeueDeskInterface-Light"), local(".LucidaGrandeUI"), local("Ubuntu Light"), local("Segoe UI Light"), local("Roboto-Light"), local("DroidSans"), local("Tahoma");
    }
    body { font-family: "system"; }
    </style>
    <style type="text/css">@media(min-width: 992px){.container{max-width: 1200px;}}</style>
  </head>
  <body class="overflow-x-hidden gridmage">
    <section class="flex flex-col w-full z-10">
      <header class="px-8 py-6 container mx-auto flex items-center justify-between mb-8">
        <a href="/" class="block">
          <img src="{{ asset('img/logo.png') }}" class="w-24" alt="Scrola Logo"/>
        </a>
        <div class="">
          <div class="flex items-center">
            <a href="/login" class="hidden md:block text-grey-darkest no-underline font-medium tracking-wide text-md no-underline">Login</a>
            <a href="/register" class="block text-grey-darkest no-underline font-medium tracking-wide text-md no-underline ml-6">Register</a>
          </div>
        </div>
      </header>
      <div class="flex-grow container mx-auto flex px-8 py-8 md:pt-10">
        <div class="md:w-2/3 w-full mx-auto">
          <h1 class="text-4xl md:text-5xl text-80 font-light mb-8 text-center leading-normal">Terms of Service</h1>
          <p class="text-md md:text-xl text-70 leading-normal mb-4 text-left">The following terms and conditions govern all use of the Scrola.app website and all content, services, and products available at or through the website. Our Services are offered subject to your acceptance without modification of all of the terms and conditions contained herein and all other operating rules, policies (including, without limitation, Scrola's Privacy Policy) and procedures that may be published from time to time by Scrola. You agree that we may automatically upgrade our Services, and these terms will apply to any upgrades. Please read this Agreement carefully before accessing or using our Services. By accessing or using any part of our services, you agree to become bound by the terms and conditions of this agreement. If you do not agree to all the terms and conditions of this agreement, then you may not access or use any of our services. If these terms and conditions are considered an offer by Scrola, acceptance is expressly limited to these terms. Our Services are not directed to children younger than 13, and access and use of our Services is only offered to users 13 years of age or older. If you are under 13 years old, please do not register to use our Services. Any person who registers as a user or provides their personal information to our Services represents that they are 13 years of age or older. Use of our Services requires a Scrola account.</p>
          <h3 class="mb-2">Your Scrola.app account</h3> 
          <p class="text-md md:text-xl text-70 leading-normal mb-4 text-left">
            If you create an account on Scrola.app, you are responsible for maintaining the security of your account, and you are fully responsible for all activities that occur under the account. You must immediately notify Scrola of any unauthorized uses of your account, or any other breaches of security. Scrola will not be liable for any acts or omissions by you, including any damages of any kind incurred as a result of such acts or omissions.
          </p>
          <h3 class="mb-2">Payment, renewal, and refunds</h3> 
          <p class="text-md md:text-xl text-70 leading-normal mb-4 text-left">
            Scrola offers different levels of service. By signing up for a particular level of service, you agree to pay Scrola the applicable subscription fees. Unless you notify us before the end of your subscription period, your subscription will renew automatically. If we change pricing for a service to which you're subscribed, we will notify you before your subscription is set to renew. You authorize us to charge any then-applicable fees to your credit card or other payment method we have on file for you. We offer refunds up to thirty (30) days after payment. Payment failures will result in the cancellation of your Scrola plan.
          </p>
          <h3 class="mb-2">Scrola.app intellectual property</h3> 
          <p class="text-md md:text-xl text-70 leading-normal mb-4 text-left">
            This Agreement does not transfer from Scrola to you any Scrola or third party intellectual property, and all right, title, and interest in and to such property will remain (as between the parties) solely with Scrola. Scrola, Scrola.app, the Scrola logo, and all other trademarks, service marks, graphics and logos used in connection with Scrola.app or our Services, are trademarks or registered trademarks of Scrola or Scrola's licensors. Other trademarks, service marks, graphics and logos used in connection with our Services may be the trademarks of other third parties. Your use of our Services grants you no right or license to reproduce or otherwise use any Scrola or third-party trademarks.
          </p>
          <h3 class="mb-2">Changes</h3> 
          <p class="text-md md:text-xl text-70 leading-normal mb-4 text-left">
            We are constantly updating our Services, and that means sometimes we have to change the legal terms under which our Services are offered. If we make changes that are material, we will let you know by posting on one of our blogs, or by sending you an email or other communication before the changes take effect. The notice will designate a reasonable period of time after which the new terms will take effect. If you disagree with our changes, then you should stop using our Services within the designated notice period. Your continued use of our Services will be subject to the new terms. However, any dispute that arose before the changes shall be governed by the Terms (including the binding individual arbitration clause) that were in place when the dispute arose.
          </p>
          <h3 class="mb-2">Disclaimer of warranties</h3> 
          <p class="text-md md:text-xl text-70 leading-normal mb-4 text-left">
            Our Services are provided "as is." Scrola and its suppliers and licensors hereby disclaim all warranties of any kind, express or implied, including, without limitation, the warranties of merchantability, fitness for a particular purpose and non-infringement. Neither Scrola nor its suppliers and licensors, makes any warranty that our Services will be error free or that access thereto will be continuous or uninterrupted.
          </p>
          <h3 class="mb-2">General representation and warranty</h3> 
          <p class="text-md md:text-xl text-70 leading-normal mb-4 text-left">
            You represent and warrant that (i) your use of our Services will be in strict accordance with the Scrola Privacy Policy, with this Agreement, and with all applicable laws and regulations (including without limitation any local laws or regulations in your country, state, city, or other governmental area, regarding online conduct and acceptable content, and including all applicable laws regarding the transmission of technical data exported from the United States or the country in which you reside) and (ii) your use of our Services will not infringe or misappropriate the intellectual property rights of any third party.
          </p>
        </div>
      </div>
    </section>    
    <footer class="text-grey-darker py-4 px-4">
      <div class="mx-auto container overflow-hidden flex md:flex-row items-center flex-col justify-between">
        <a href="/" class="block md:mr-4">
          <img src="{{ asset('img/logo.png') }}" class="w-24" alt="Scrola Logo">
        </a>
        <div class="w-full md:w-1/2 my-4 md:my-0 flex text-sm justify-center">
          <a href="/tos" class="block no-underline">
            <ul class="text-grey-dark list-reset font-thin flex flex-col text-left">
              <li class="inline-block py-2 px-3 text-grey-darkest no-underline font-medium tracking-wide">Terms of Service</li>
            </ul>
          </a>
          <a href="https://mantalabs.co/work/scrola" class="block no-underline">
            <ul class="text-grey-dark list-reset font-thin flex flex-col text-left">
              <li class="inline-block py-2 px-3 text-grey-darkest no-underline font-medium tracking-wide">About</li>
            </ul>
          </a>
        </div>
        <a href="https://mantalabs.co" class="block no-underline">
          <p class="text-grey-darkest"> ©{{ date('Y') }} Mantalabs</p>
        </a>
      </div>
    </footer>
  <body>
</html>
