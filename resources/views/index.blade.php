<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
		<meta charset="utf-8">
		<meta name="title" content="Scrola | Create Scrolling, Website Screenshots">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="crola allows web designers and developers to create scrolling screenshots of websites in just a few clicks. Simply select the viewport, and provide a URL or your own screenshot.">
		<!-- Open Graph / Facebook -->
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://scrola.app/">
		<meta property="og:title" content="Scrola | Create Scrolling, Website Screenshots">
		<meta property="og:description" content="Scrola allows web designers and developers to create scrolling screenshots of websites in just a few clicks. Simply select the viewport, and provide a URL or your own screenshot.">
		<meta property="og:image" content="https://scrola.app/img/mt.jpg">
		<!-- Twitter -->
		<meta property="twitter:card" content="summary_large_image">
		<meta property="twitter:url" content="https://scrola.app/">
		<meta property="twitter:title" content="Scrola | Create Scrolling, Website Screenshots">
		<meta property="twitter:description" content="Scrola allows web designers and developers to create scrolling screenshots of websites in just a few clicks. Simply select the viewport, and provide a URL or your own screenshot.">
		<meta property="twitter:image" content="https://scrola.app/img/mt.jpg">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <title>Scrola | Create Scrolling, Website Screenshots</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css" />
    <style>
    @font-face {
      font-family: system;
      font-style: normal;
      font-weight: 300;
      src: local(".SFNSText-Light"), local(".HelveticaNeueDeskInterface-Light"), local(".LucidaGrandeUI"), local("Ubuntu Light"), local("Segoe UI Light"), local("Roboto-Light"), local("DroidSans"), local("Tahoma"), local('HelveticaNeue');
    }
    body { font-family: "system"; }
    </style>
    <style type="text/css">@media(min-width: 992px){.container{max-width: 1200px;}}</style>
  </head>
  <body class="overflow-x-hidden gridmage">
    <section class="flex flex-col w-full z-10">
      <header class="px-8 py-6 container mx-auto flex items-center justify-between mb-8 w-full">
        <a href="/" class="block">
          <img src="{{ asset('img/logo.png') }}" class="w-24" alt="Scrola Logo"/>
        </a>
        <div class="">
          <div class="flex items-center">
            <a href="/login" class="hidden md:block text-grey-darkest no-underline font-medium tracking-wide text-md no-underline">Login</a>
            <a href="/register" class="block text-grey-darkest no-underline font-medium tracking-wide text-md no-underline ml-6">Register</a>
          </div>
        </div>
      </header>
      <div class="w-full flex-grow container mx-auto flex flex-col md:flex-row flex-row-reverse md:flex-row md:items-center px-8 py-8 md:pt-10">
        <div class="md:w-1/2 w-full md:mr-10 mr-0">
          <h1 class="text-4xl md:text-5xl text-80 font-light mb-8 md:text-left text-center leading-normal">Create scrolling screenshots with ease</h1>
          <p class="text-md md:text-xl text-70 leading-normal mb-10 text-left">
            It is now incredibly easy to generate scrolling screenshots and give life to your designs; all without After Effects. Start showing off your work and impressing your clients! It's free and easy to get started.
          </p>
          <div class="flex flex-wrap">
            <a href="#learn" class="no-underline block mr-4 mb-4 md:mb-0">
              <button class="block border border-transparent bg-blue-dark font-normal text-white py-3 px-4 rounded outline-none focus:outline-none">
                Learn more
              </button>
            </a>
            <a href="/register" class="no-underline block">
              <button class="block border border-blue-dark font-normal text-blue-dark py-3 px-4 rounded outline-none focus:outline-none">
                Create an account
              </button>
            </a>
          </div>
        </div>
        <div class="flex-grow md:w-1/2 w-full hidden md:block">
          <video class="shadow-lg h-full md:h-auto w-full md:w-auto rounded" loop muted autoplay>
            <source src="{{ asset('img/desktop.webm') }}" type="video/webm">
            <source src="{{ asset('img/desktop.mp4') }}" type="video/mp4">
          </video>
        </div>
      </div>
    </section>
    <section class="w-full z-10 py-8" id="learn" style="background-color: rgba(223, 226, 231, .4);">
      <h2 class="text-3xl md:text-5xl text-80 my-0 font-light text-center leading-normal">How It Works</h2>
      <div class="flex-grow container mx-auto flex flex-col md:flex-row flex-row-reverse md:flex-row md:items-center px-8 py-8 md:pt-10">
        <div class="md:w-1/2 w-full md:mr-10 mr-0 hidden md:block">
          <video class="shadow-lg h-full md:h-auto w-full rounded""
            loop muted autoplay>
            <source src="{{ asset('img/tablet.webm') }}" type="video/webm">
            <source src="{{ asset('img/tablet.mp4') }}" type="video/mp4">
          </video>
        </div>
        <div class="flex-grow md:w-1/2 w-full">
          <div class="shadow-lg bg-white border p-8 rounded mb-6">
            <p class="text-md md:text-xl text-70 leading-normal mb-4 font-normal text-left">
              1. Select an image source
            </p>
            <p class="text-md md:text-xl text-70 leading-normal text-left">
              Simply enter the URL of the page you would like to capture, or provide your own screenshot.
            </p>
          </div>
          <div class="shadow-lg bg-white border p-8 rounded mb-6">
            <p class="text-md md:text-xl text-70 leading-normal mb-4 font-normal text-left">
              2. Customize your shot
            </p>
            <p class="text-md md:text-xl text-70 leading-normal text-left">
              Make your screenshot perfect by customizing the scroll speed, viewport, background color, and more.
            </p>
          </div>
          <div class="shadow-lg bg-white border p-8 rounded mb-6">
            <p class="text-md md:text-xl text-70 leading-normal mb-4 font-normal text-left">
              3. Render and celebrate
            </p>
            <p class="text-md md:text-xl text-70 leading-normal text-left">
              Sit back and leave the rest to us. Most screenshots are rendered in a few minutes.
            </p>
          </div>
        </div>
        {{-- <video class="shadow-lg w-full rounded""
          loop muted autoplay>
          <source src="{{ asset('img/tablet.webm') }}" type="video/webm">
          <source src="{{ asset('img/tablet.mp4') }}" type="video/mp4">
        </video> --}}
      </div>
    </section>
    <footer class="text-grey-darker py-4 px-4">
      <div class="mx-auto container overflow-hidden flex md:flex-row items-center flex-col justify-between">
        <a href="/" class="block md:mr-4">
          <img src="{{ asset('img/logo.png') }}" class="w-24" alt="Scrola logo">
        </a>
        <div class="w-full md:w-1/2 my-4 md:my-0 flex text-sm justify-center">
          <a href="/tos" class="block no-underline">
            <ul class="text-grey-dark list-reset font-thin flex flex-col text-left">
              <li class="inline-block py-2 px-3 text-grey-darkest no-underline font-medium tracking-wide">Terms of Service</li>
            </ul>
          </a>
          <a href="https://mantalabs.co/work/scrola" class="block no-underline">
            <ul class="text-grey-dark list-reset font-thin flex flex-col text-left">
              <li class="inline-block py-2 px-3 text-grey-darkest no-underline font-medium tracking-wide">About</li>
            </ul>
          </a>
        </div>
        <a href="https://mantalabs.co" class="block no-underline">
          <p class="text-grey-darkest"> ©{{ date('Y') }} Mantalabs</p>
        </a>
      </div>
    </footer>
  <body>
</html>
