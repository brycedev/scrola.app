<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <title>Scrola | 404</title>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400,600" rel="stylesheet" type="text/css">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <style type="text/css">@media(min-width: 992px){.container{max-width: 1200px;}}</style>
  </head>
  <body class="overflow-hidden max-h-full h-full w-full gridmage border-16 border-blue-dark flex flex-col">
    <header class="px-8 py-6 container mx-auto flex items-center justify-between animated fadeInDown mb-8">
      <a href="/"><img src="{{ asset('img/logo.png') }}" class="w-24"></a>
      <div class="">
        <div class="flex items-center">
          <a href="/login" class="hidden md:block text-grey-darkest no-underline font-medium tracking-wide text-md no-underline">Login</a>
          <a href="/register" class="block text-grey-darkest no-underline font-medium tracking-wide text-md no-underline ml-6">Register</a>
        </div>
      </div>
    </header>
    <div class="flex-grow flex items-center justify-center">
      <div class="w-full">
        <h1 class="text-center text-blue-dark pb-10 font-thin" style="font-size: 300px;">404</h1>
      </div>
    </div>
  </body>
</html>
