@extends('layouts.auth')

@section('content')
<div class="w-full max-w-md flex flex-col justify-center p-4">
  <h3 class="text-3xl md:text-5xl text-80 font-thin mb-8 text-center leading-normal mb-8">Welcome back!</h3>
  <div class="card-body">
    @if ($errors->has('email') || $errors->has('password'))
      <div class="mb-4 text-sm mx-auto table px-4 py-2 text-white rounded-full bg-red-light font-normal" role="alert">
        Whoops! Please try again.
      </div>
    @endif
    <form method="POST" action="{{ route('login') }}" class="mb-8">
      @csrf
      <div class="mb-4">
        <label for="email" class="font-normal">Email Address</label>
        <div class="w-full mt-2">
          <input id="email" type="email" class="mb-2 bg-khaki text-grey-darkest appearance-none border rounded w-full py-2 px-4 focus:outline-none focus:bg-white" name="email" value="{{ old('email') }}"
            required autofocus>
        </div>
      </div>
      <div class="mb-4">
        <label for="password" class="font-normal mb-2">Password</label>
        <div class="w-full mt-2">
          <input id="password" type="password" class="bg-khaki text-grey-darkest appearance-none border rounded w-full py-2 px-4 focus:outline-none focus:bg-white" name="password"
            required>
        </div>
      </div>
      <button type="submit" class="block border border-transparent bg-blue font-normal text-white py-3 px-4 rounded outline-none focus:outline-none w-full mb-6">
        Login
      </button>
      <a class="table mx-auto text-blue underline font-normal" href="{{ route('password.request') }}">
          {{ __('Forgot Your Password?') }}
      </a>
    </form>
  </div>
</div>
@endsection
