@extends('layouts.auth')

@section('content')
<div class="w-full max-w-md flex flex-col justify-center p-4">
  <h3 class="text-3xl md:text-5xl text-80 font-thin mb-8 text-center leading-normal mb-8">Register</h3>
  <div class="card-body">
    @if ($errors->has('name'))
      @if($errors->first('name') === 'validation.min.string')
        <div class="mb-4 text-sm mx-auto table px-4 py-2 text-white rounded-full bg-red-light font-normal" role="alert">
          Your name / company name must be at least 3 characters long.
        </div>
      @endif
    @endif
    @if ($errors->has('email'))
      @if($errors->first('email') === 'validation.unique')
        <div class="mb-4 text-sm mx-auto table px-4 py-2 text-white rounded-full bg-red-light font-normal" role="alert">
          This email address is already taken.
        </div>
      @endif
    @endif
    @if ($errors->has('password'))
      @if($errors->first('password') === 'validation.min.string')
        <div class="mb-4 text-sm mx-auto table px-4 py-2 text-white rounded-full bg-red-light font-normal" role="alert">
          Your password must be at least 8 characters long.
        </div>
      @endif
    @endif
    @if ($errors->has('password'))
      @if($errors->first('password') === 'validation.confirmed')
        <div class="mb-4 text-sm mx-auto table px-4 py-2 text-white rounded-full bg-red-light font-normal" role="alert">
          Your passwords do not match.
        </div>
      @endif
    @endif
    <form method="POST" action="{{ route('register') }}" class="mb-8">
      @csrf
      <div class="mb-4">
        <label for="email" class="font-normal">Name / Company Name</label>
        <div class="w-full mt-2">
          <input id="name" type="text" class="mb-2 bg-khaki text-grey-darkest appearance-none border rounded w-full py-2 px-4 focus:outline-none focus:bg-white"
            name="name" value="{{ old('name') }}" required autofocus>
        </div>
      </div>
      <div class="mb-4">
        <label for="email" class="font-normal">Email Address</label>
        <div class="w-full mt-2">
          <input id="email" type="email" class="mb-2 bg-khaki text-grey-darkest appearance-none border rounded w-full py-2 px-4 focus:outline-none focus:bg-white"
            name="email" value="{{ old('email') }}" required autofocus>
        </div>
      </div>
      <div class="mb-4">
        <label for="password" class="font-normal mb-2">Password</label>
        <div class="w-full mt-2">
          <input id="password" type="password" class="bg-khaki text-grey-darkest appearance-none border rounded w-full py-2 px-4 focus:outline-none focus:bg-white"
            name="password" required> @if ($errors->has('password'))
          <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span> @endif
        </div>
      </div>
      <div class="mb-4">
        <label for="password-confirm" class="font-normal mb-2">Confirm Password</label>

        <div class="w-full mt-2">
          <input id="password-confirm" type="password" class="bg-khaki text-grey-darkest appearance-none border rounded w-full py-2 px-4 focus:outline-none focus:bg-white"
            name="password_confirmation" required>
        </div>
      </div>
      <button type="submit" class="block border border-transparent bg-blue font-normal text-white py-3 px-4 rounded outline-none focus:outline-none w-full mb-6">
        Register
      </button>
      </a>
    </form>
  </div>
</div>
@endsection
